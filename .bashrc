#
# ~/.bashrc
#

#If not running interactively, don't do anything
[[ $- != *i* ]] && return

export TERM=xterm-256color

export HISTSIZE=-1
#export HISTCONTROL=erasedups

export VITASDK=$HOME/.vitasdk

export RENPY_DEPS_INSTALL=$VITASDK/arm-vita-eabi
export RENPY_STATIC=1
export PYGAME_SDL2_STATIC=1

export PIPPKGS=$HOME/.local/bin
export SCRIPTS=$HOME/Documents/git/awful-shell-scripts/\s\h
export PATH=$VITASDK/bin:$PATH:$SCRIPTS:$PIPPKGS

#coloured man pages by oyvindio
export LESS_TERMCAP_mb=$'\E[01;31m'
export LESS_TERMCAP_md=$'\E[01;31m'
export LESS_TERMCAP_me=$'\E[0m'
export LESS_TERMCAP_se=$'\E[0m'
export LESS_TERMCAP_so=$'\E[01;44;33m'
export LESS_TERMCAP_ue=$'\E[0m'
export LESS_TERMCAP_us=$'\E[01;32m'

#shopt
shopt -s autocd
shopt -s checkwinsize
shopt -s cmdhist
shopt -s extglob

user_agent='Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/126.0.0.0 Safari/537.36'

hue='\e[1m\e[31m'
ending='\e[0m\e[22m'

#export HADOOP_HOME=$HOME/Documents/hadoop
#export JAVA_HOME=/usr/lib/jvm/java-8-openjdk

#enable conda
[ -f /opt/miniconda3/etc/profile.d/conda.sh ] && source /opt/miniconda3/etc/profile.d/conda.sh

echo -e "$hue[$(date '+%F %T') $(uptime -p)] [./${PWD##*/}]$ending"

#misc
alias ls='ls --color=auto'
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'
alias where='pwd -L; ls -lFia'
alias sf='neofetch'
alias rmdir='rmdir -v'
alias hexedit='hexedit --color'
alias {anm,amn}='man'
alias space='du -hSc | sort -hr >> ua'
alias {poweroff,poweroof}='shutdown -P now'

alias gsh='gnome-shell --wayland'
alias xfce='startxfce4'

#hyprland
alias {hpr,hpld}='Hyprland'
alias hpcl='hyprctl clients'

#clamav
alias {clamup,clam-up,clam-update}='freshclam -v --show-progress'
alias {scan,cscan}='clamscan --bytecode --heuristic-alerts --scan-pe --scan-elf --scan-ole2 --scan-pdf --scan-swf --scan-html --scan-archive --alert-encrypted --alert-macros -ro'

#ifconfig
alias ifc='ifconfig'
alias ifca='ifconfig -a'
alias ifcs='ifconfig -s'

#pacman
alias pacup='sudo pacman -Syu'
alias paclr='sudo rm /var/cache/pacman/pkg/*.tar*'

#flatpak
alias {flap,flaup}='flatpak update'
alias flaun='flatpak uninstall --unused -y'
alias flals='flatpak list'
alias flare='flatpak repair'

#wget
alias {wget,wge}="wget --retry-on-host-error -U '$user_agent'"
alias wgetdir='wget -c -cmN -nH --no-parent'
alias {wgdr,wgtdir}='wget -c -r --no-parent'

wgl() {
  query=(${1//\?/ });
  echo "$query";
  wge "$query";
}

#aria2
alias {a2c,aria2}="aria2c -m 10 --retry-wait=2 -U '$user_agent'"
alias a2zip='a2c --http-accept-gzip true'
alias a2='a2c -c -j 12 -x 12'

#curl
alias cur='curl --retry-connrefused --show-error -f -O'
alias curu="cur -A '$user_agent'"
alias curz='cur -Z --parallel-immediate --parallel-max 3 --retry 0'

#ffmpeg
alias {spek,spec}='for i in *.{flac,m4a,mp3,wav,aif*,mp4,mkv,mov,ogg}; do ffmpeg -i "$i" -lavfi showspectrumpic "${i%.*}.png"; done'
alias {stripmp3,ripmp3}='for i in *.mkv; do ffmpeg -i "$i" -map 0:1 -c:a copy "${i%.*}.mp3"; done'
alias {stripm4a,ripm4a}='for i in *.mp4; do ffmpeg -i "$i" -map 0:1 -c:a copy "${i%.*}.m4a"; done'
alias {stripopus,ripopus}='for i in *.{webm,mkv}; do ffmpeg -i "$i" -map 0:1 -c:a copy "${i%.*}.1.webm"; done'
alias {stripvideo,ripvideo}='for i in *.{mp4,webm,mov}; do ffmpeg -i "$i" -map 0:0 -c:v copy "${i%.*}.mkv"; done'

#file conversion
alias {mp4tomov,mp42mov}='for i in *.mp4; do ffmpeg -i "$i" -vcodec mjpeg -q:v 2 -acodec pcm_s16be -q:a 0 -f mov "${i%.*}.mov"; done; rm *.mp4'
alias {movtomp4,mov2mp4}='for i in *.mov; do ffmpeg -i "$i" "${i%.*}.mp4"; done; rm *.mov'
alias {giftomp4,gif2mp4}='for i in *.gif; do ffmpeg -i "$i" "${i%.*}.mp4"; done; rm *.gif'
alias {mkvtomp4,mkv2mp4}='for i in *.mkv; do ffmpeg -i "$i" "${i%.*}.mp4"; done;'
alias {mkvtomp3,mkv2mp3}='for i in *.mkv; do ffmpeg -i "$i" -ab 256k "${i%.*}.mp3"; done; rm *.mkv'
alias {mp4tomp3,mp42mp3}='for i in *.mp4; do ffmpeg -i "$i" -ab 256k "${i%.*}.mp3"; done; rm *.mp4'
alias {wavtomp3,wav2mp3}='for i in *.wav; do ffmpeg -i "$i" -ab 320k "${i%.*}.mp3"; done'
alias {webptopng,webp2png}='for i in *.webp; do convert "$i" "${i%.*}.png"; done; rm *.webp'
alias {pdftopng,pdf2png}='gs -dSAFER -dBATCH -dNOPAUSE -r150 -sDEVICE=png16m -dTextAlphaBits=4 -sOutputFile=page-%02d.png'

#hash
alias md5='md5sum'
alias sha256='sha256sum'
alias sha384='sha384sum'
alias sha512='sha512sum'

b64d() {
  echo "$1" | base64 -di;
  echo;
}

b64e() {
  echo "$1" | base64;
  echo;
}

findr() {
  find / -iname "$1" 2> /dev/null;
}

shadir() {
  for d in */ ; do
    echo "$d"; 
    cd "$_"; 
    sha256sum *.* > sha256; 
    cd ..; 
  done
}

PS1="$hue>> $ending"

#WIDTH------------------------------------------------------------------------------------
