#
# ~/.bash_profile
#

# force the use of GBM as backend for nvidia:
export GBM_BACKEND=nvidia-drm
export __GLX_VENDOR_LIBRARY_NAME=nvidia-open

# select the first gpu:
#gpu=$(udevadm info -a -n /dev/dri/card1 | grep boot_vga | rev | cut -c 2)
export WLR_DRM_DEVICES="/dev/dri/card2"

export GDK_BACKEND=wayland

[[ -f ~/.bashrc ]] && . ~/.bashrc